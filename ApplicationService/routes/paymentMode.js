var express = require('express');
var mongoose = require('mongoose');
mongoose.connect('mongodb://192.168.99.100/pathDB'); //mongodb Server and Database information. pathDB is the name of the database

//Your schema defination. PaymentModes is the table / collection name inside PathDb database
var paymentModeSchema = mongoose.model('PaymentModes', {name: String, description: String});

//router is used to expose your api to external world
var router = express.Router();

//This will allow cross browser access to our API
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/* GET home page. */
router.get('/getAll', function(req, res, next) {
    //paymentModeSchema will use Find api and return the data which fits its criteria. I have left the criteria
    //blank for getting all the records from the table.
    paymentModeSchema.find({},function(err, docs){
        //response is returned JSON format
        res.json(docs);
        // we need to end to response.
        res.end();
    });
});

//expose your api to external world
module.exports = router;
